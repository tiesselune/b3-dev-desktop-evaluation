import Paint from "./Paint.js";

const paint = new Paint(
    document.querySelector("canvas"),
    document.querySelector("#colors"),
    document.querySelector("#toolbox")
);