
const colors = ["#000","#fff","#3584e4","#1c71d8","#1a5fb4","#33d17a","#2ec27e","#26a269","#f8e45c","#f6d32d","#f5c211","#ff7800","#e66100", "#c64600", "#e01b24", "#c01c28", "#a51d2d", "#9141ac", "#813d9c","#613583"]
const tools = [2,5,7,9,12,20];


/**
 * Class Paint : defines paint program.
 */
export default class Paint {

    /**
     * Paint program constructor
     * @param {HTMLCanvasElement} canvas The canvas used for drawing
     * @param {HTMLElement} colors Element to draw the palette in
     * @param {HTMLElement} toolbox Element to draw the toolbox in
     */
    constructor(canvas,colors,toolbox){
        this.canvas = canvas;
        this.context = canvas.getContext("2d");
        this.colors = colors;
        this.toolbox = toolbox;
        this.drawing = false;
        this.currentPosition = {x : 0, y : 0};
        this.previousPosition = null;
        this.constructColors();
        this.constructToolbox();
        this.setupCanvas();
        this.currentColor = "#000";
        this.currentTool = 2;
    }

    /**
     * Gets PNG data from the current image as PNG
     * @returns {string} base64 representation of the PNG file as a string
     */
    getPNGData(){
        const url = this.canvas.toDataURL('image/png');
        return url.substring(url.indexOf(",") + 1);
    }

    /**
     * Draws image at top left of drawing area
     * @param {string} base64Data PNG data as a b64 encoded string
     */
    drawPNGData(base64Data){
        let image = new Image();
        image.addEventListener("load", () => {
            this.context.drawImage(image,0,0);
        });
        image.src = "data:image/png;base64," + base64Data;
    }

    /**
     * Constructs palette from colors
     */
    constructColors(){
        for(let color of colors){
            this.makeColor(color);
        }
    }

    /**
     * Constructs toolbox from tool sizes
     */
    constructToolbox(){
        for(let tool of tools){
            this.makeTool(tool);
        }
    }

    /**
     * Sets up the canvas for drawing
     */
    setupCanvas(){
        this.context.fillStyle = "#fff";
        this.context.fillRect(0,0,this.canvas.width,this.canvas.height);
        this.changeCanvasSize();
        window.addEventListener("resize",() => {
            this.changeCanvasSize();
        });
        this.canvas.addEventListener("mousedown",(e) => {
            this.drawing = true;
            this.currentPosition = this.getMousePosition(e);
            this.drawCircle(this.currentPosition.x,this.currentPosition.y);
        });
        this.canvas.addEventListener("mouseup",(e) => {
            this.drawing = false;
            this.drawCircle(this.currentPosition.x,this.currentPosition.y);
            this.previousPosition = null;
        });
        this.canvas.addEventListener("mousemove",(e) => {
            if(!this.drawing){
                return;
            }
            let newPosition = this.getMousePosition(e);
            this.context.beginPath();
            if(this.previousPosition){
                this.context.moveTo(this.previousPosition.x, this.previousPosition.y);
                this.context.lineTo(this.currentPosition.x,this.currentPosition.y);
            }
            else {
                this.context.moveTo(this.currentPosition.x, this.currentPosition.y);
            }
            this.context.lineTo(newPosition.x,newPosition.y);
            this.context.stroke();
            this.previousPosition = this.currentPosition;
            this.currentPosition = newPosition;
        });
    }

    /**
     * Draws a circle of the current color and current tool size
     * @param {number} x x coordinate of the center of the circle
     * @param {number} y y coordinate of the center of the circle
     */
    drawCircle(x,y) {
        this.context.beginPath();
        this.context.arc(x, y, this.currentTool/2, 0, 2 * Math.PI, false);
        this.context.fillStyle = this.currentColor;
        this.context.fill();
    }

    /**
     * Adatps canvas size depending on window size
     */
    changeCanvasSize(){
        const content = this.context.getImageData(0,0,this.canvas.width,this.canvas.height)
        const rect = this.canvas.getBoundingClientRect();
        this.canvas.width = rect.width;
        this.canvas.height = rect.height;
        this.context.fillStyle = "#fff";
        this.context.fillRect(0,0,this.canvas.width,this.canvas.height);
        this.context.putImageData(content,0,0);
    }

    /**
     * Creates a palette element from a CSS color string and appends it to the DOM
     * @param {string} color CSS color
     * @returns {HTMLDivElement} A clickable div for the palette
     */
    makeColor(color){
        const colorElem = document.createElement("div");
        colorElem.classList.add("color");
        colorElem.style.backgroundColor = color;
        colorElem.addEventListener("click", () => {
            this.currentColor = color;
        })
        this.colors.appendChild(colorElem);
        return colorElem;
    }

    /**
     * Creates a toolbox element from a brush radiusand appends it to the DOM
     * @param {number} tool Brush radius
     * @returns {HTMLDivElement} A clickable div for the toolbox
     */
    makeTool(tool){
        const toolElem = document.createElement("div");
        toolElem.classList.add("tool");
        const toolPointer = document.createElement("div");
        toolPointer.classList.add("toolPointer");
        toolElem.addEventListener("mouseenter", () => {
            toolPointer.style.backgroundColor = "white";
            toolElem.style.backgroundColor = this.currentColor;
        });
        toolElem.addEventListener("mouseleave", () => {
            toolPointer.style.backgroundColor = this.currentColor;
            toolElem.style.backgroundColor = "inherit";
        });
        toolElem.addEventListener("click", () => {
            this.currentTool = tool;
        })
        toolPointer.style.height = tool + "px";
        toolPointer.style.width = tool + "px";
        toolElem.appendChild(toolPointer);
        this.toolbox.appendChild(toolElem);
        return toolElem;
    }

    get currentColor(){
        return this._currentColor;
    }
    set currentColor(value){
        this._currentColor = value;
        this.context.strokeStyle = value;
        const tools = document.querySelectorAll(".toolPointer");
        for(let tool of tools){
            tool.style.border = value == "#fff" ? "solid 1px black" : "none";
            tool.style.backgroundColor = this._currentColor;
        }
    }
    get currentTool(){
        return this._currentTool;
    }
    set currentTool(value){
        this.context.lineWidth = value;
        this._currentTool = value;
    }

    /**
     * Computes mouse coordinates on the canvas
     * @param {MouseEvent} mouseEvent 
     * @returns {{x : number, y : number}} the coordinates of the mouse on the canvas
     */
    getMousePosition(mouseEvent){
        var x = mouseEvent.pageX - this.canvas.offsetLeft;
        var y = mouseEvent.pageY - this.canvas.offsetTop;

        return {x,y};
    }


}

