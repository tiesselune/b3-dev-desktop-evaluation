# B3 Developpement Desktop : Evaluation

---

## 1. Principe de l'évaluation

Le but de ce projet est de réaliser une petite application de type *paint*.

Le code de l'interface vous est déjà fourni dans le dossier `front`, il s'agira d'en mettre en place la fenêtre et les fonctionnalités d'ouverture et de sauvegarde ainsi que la communication avec le Main Process d'Electron.

:bulb: Vous pouvez tester `index.html` dans un serveur local!

Vous devrez, pour réussir ce TP :
 - Compléter ou créer les fichiers nécessaires (`main.js`) notamment pour afficher une fenêtre affichant l'interface du programme Paint (`front/index.html`).
 - Permettre l'ouverture et la sauvegarde de fichiers PNG depuis un dialogue natif déclenché par un menu
 - Respecter les contraintes listées ci-dessous
 - Valider les tests unitaires du programme, écrits dans le dossier `tests` et lançables avec `npm run test`


🚧 Vous  n'avez pas besoin de comprendre l'ensemble du code fourni dans `front`, mis à part quelques informations données ci-dessous.
☝😉 Pensez à lire les tests si vous n'arrivez pas à les valider, même si le respect scrupuleux des contraintes ci-dessous devrait suffire.

---

## 2. Contraintes

 - La taille **minimum** de la fenêtre doit être de 800 par 600 pixels (il ne doit pas être possible de la redimentionner en dessous)
 - L'icone de la fenêtre devra être le fichier `assets/icon.png`
 - Le menu de l'application doit faire apparaître *au moins* un menu `Fichier` contenant les entrées suivantes (au caractère près pour valider les tests) :
    - `Ouvrir...` permettant d'ouvrir un fichier (avec le raccourci clavier `Ctrl+O`)
    - `Enregistrer sous...` permettant de sauvegarder le canvas dans un fichier PNG (avec le raccourci clavier `Ctrl+S`)
    - Ces deux éléments ouvriront un dialogue natif limité aux formats de fichiers `.png` (on ne verra pas les fichiers ayant d'autres extensions) et ouvert avec les versions asynchrones `showSaveDialog` et `showOpenDialog` (les version `Sync` ne valident pas les tests)
 - Ouvrir un fichier PNG le dessinera à l'écran (dans le coin supérieur gauche du canvas) à l'aide de la méthode `drawPNGData` de la classe `Paint` (dans `Paint.js`)
 - Sauvegarder un fichier récupèrera les données du canvas avec `getPNGData` de la classe `Paint` (dans `Paint.js`) et les sauvegardera à l'endroit choisi par l'utilisateur.
 - Vous ne devez pas utiliser les modules découragés comme `remote`, désactiver `contextIsolation` ou activer `nodeIntegration`. Vous devrez à la place utiliser IPC.
 - Vous devrez prendre en compte les différences de comportement entre Mac et Windows (fermeture du programme et ordre des menus)

---

## 3. Astuces

 - La classe `Paint` contient deux méthodes que vous devrez utiliser :
    - `drawPNGData` qui prend en argument une chaîne en base64 représentant les données d'une image PNG
    - `getPNGData` qui renvoie la chaîne base64 du contenu du canvas
 - Vous pouvez lire/écrire un fichier directement en base64 en ajoutant `{encoding : "base64"}` en option lorsque vous lisez/sauvegardez le fichier avec NodeJS.
 - Transmettre des données du *RendererProcess* à son Preload Script se fait avec un callback. Vous pouvez vérifier les exemples mis en place en cours.
 - Vous pouvez rajouter les outils développeur au menu avec `{role : "toggleDevTools"}` si vous avez besoin de débugger le RendererProcess.

---

## 4. Lancer les tests

Installez toutes les dépendances avant de lancer les tests.

```bash
npm install
npm run test
```

---

## 5. Rendu

Rendez votre code sur Teams sous la forme d'une archive zip **n'incluant pas `node_modules`** mais incluant le reste de vos fichiers