const { _electron: electron } = require('playwright')
const { test, expect } = require('@playwright/test');
const { app } = require('electron');
const {join,resolve} = require("path");
const {tmpdir} = require("os");
const {mkdir,rm,access,open,writeFile,readFile} = require("fs/promises");
const {PNG} = require("pngjs");

const TestDir = join(tmpdir(),"painttest");
const TestImagePath = join(TestDir,"test.png");
const iconPath = resolve(__dirname,"../assets/icon.png");

let electronApp = null;
let window = null;

test.describe("Cross-platform testing", () => {
    test.beforeAll(async () => {
        await mkdir(TestDir);
        electronApp = await electron.launch({ args: ['.'] });
        electronApp.evaluate( async ({app}) => {
            app.getMenuItem = (menu,name) => {
                for(let item of menu.items){
                    if(item.label.toLowerCase() == name.toLowerCase()){
                        return item;
                    }
                }
                return null;
            };
            app.getSubMenu = (menu,name,subname) => {
                let parent = app.getMenuItem(menu,name);
                if(!parent){
                    return null;
                }
                return app.getMenuItem(parent.submenu,subname);
            };
        });
        window = await electronApp.firstWindow();
    });
    
    test.afterAll(async () => {
        await rm(TestDir,{recursive : true, force : true});
        electronApp.close();
        electronAPp = null;
        window = null;
    });
    
    test.describe("Basic window tests", () => {
        test("should create window", async () => {
            expect( window).not.toBeNull();
        });
        test("should set window Title to \"Paint\"", async () => {
            expect(await window.title()).toBe("Paint");
        });
        test("should have initial size of 800x600", async () => {
            const size = await electronApp.evaluate(async ({BrowserWindow}) => {
                return BrowserWindow.getFocusedWindow().getSize()
            });
            expect(size[0]).toBeGreaterThanOrEqual(800);
            expect(size[1]).toBeGreaterThanOrEqual(600);
        });
        test("should have minimum size of 800x600", async () => {
            const size = await electronApp.evaluate(async ({BrowserWindow}) => {
                BrowserWindow.getFocusedWindow().setSize(400,300);
                return BrowserWindow.getFocusedWindow().getSize()
            });
            expect(size[0]).toBeGreaterThanOrEqual(800);
            expect(size[1]).toBeGreaterThanOrEqual(600);
        });
        test("should load application correctly", async () => {
            const url = await electronApp.evaluate(async ({BrowserWindow}) => {
                return BrowserWindow.getFocusedWindow().webContents.getURL();
            });
            expect(url).toContain("front/index.html");
        });
    });
    
    test.describe("Application Menu", () => {
        test("should set up menu correctly with an entry named 'Fichier'", async () => {
            const hasFileMenu = await electronApp.evaluate(async ({app,Menu}) => {
                return app.getMenuItem(Menu.getApplicationMenu(),"Fichier") !== null;
            });
            expect(hasFileMenu).toBeTruthy();
        });
        test("should have an 'Ouvrir...' submenu with correct shortcut", async () => {
            const openMenu = await electronApp.evaluate(async ({app,Menu}) => {
                const openMenu = app.getSubMenu(Menu.getApplicationMenu(),"Fichier","Ouvrir...")
                return { hasOpenMenu : openMenu !== null, hasOpenShortcut : openMenu !== null && ["cmd+o","ctrl+o"].includes(openMenu.accelerator.toLowerCase())};
            });
            expect(openMenu.hasOpenMenu).toBeTruthy();
            expect(openMenu.hasOpenShortcut).toBeTruthy();
        });
        test("should have an 'Enregistrer sous...' submenu with correct shortcut", async () => {
            const saveMenu = await electronApp.evaluate(async ({app,Menu}) => {
                const sMenu = app.getSubMenu(Menu.getApplicationMenu(),"Fichier","Enregistrer sous...")
                return { hasSaveMenu : sMenu !== null, hasSaveShortcut :sMenu !== null && ["cmd+s","ctrl+s"].includes(sMenu.accelerator.toLowerCase())};
            });
            expect(saveMenu.hasSaveMenu).toBeTruthy();
            expect(saveMenu.hasSaveShortcut).toBeTruthy();
        });
    });
    
    test.describe("Save and Open Actions", () => {
        test("Open should show Open Dialog with `showOpenDialog`", async () => {
            const openDialog = await setupDialogFunction("showOpenDialog","Ouvrir...",true,"");
            expect(openDialog.hasBeenCalled).toBe(true);
        });
        test("Save should show Save Dialog with `showSaveDialog`", async () => {
            const saveDialog = await setupDialogFunction("showSaveDialog","Enregistrer sous...",true,"");
            expect(saveDialog.hasBeenCalled).toBe(true);
        });
        test("Open Dialog should be parented to main window", async () => {
            const openDialog = await setupDialogFunction("showOpenDialog","Ouvrir...",true,"");
            expect(openDialog.parented).toBe(true);
        });
        test("Save Dialog should be parented to main window", async () => {
            const saveDialog = await setupDialogFunction("showSaveDialog","Enregistrer sous...",true,"");
            expect(saveDialog.parented).toBe(true);
        });
        test("SaveDialog have filters set to PNG", async () => {
            const saveDialog = await setupDialogFunction("showSaveDialog","Enregistrer sous...",true,"");
            expect(saveDialog.options.filters).toBeTruthy();
            expect(saveDialog.options.filters.length).toBeGreaterThanOrEqual(1);
            expect(saveDialog.options.filters[0].extensions).toBeTruthy();
            expect(saveDialog.options.filters[0].extensions.length).toBeGreaterThanOrEqual(1);
            expect(saveDialog.options.filters[0].extensions[0]).toContain("png");
        });
        test("OpenDialog have filters set to PNG", async () => {
            const openDialog = await setupDialogFunction("showOpenDialog","Ouvrir...",true,"");
            expect(openDialog.options.filters).toBeTruthy();
            expect(openDialog.options.filters.length).toBeGreaterThanOrEqual(1);
            expect(openDialog.options.filters[0].extensions).toBeTruthy();
            expect(openDialog.options.filters[0].extensions.length).toBeGreaterThanOrEqual(1);
            expect(openDialog.options.filters[0].extensions[0]).toContain("png");
        });
    });
    
    test.describe("Saving PNG file", () => {
        test("Using \"Enregistrer sous...\" menu should save PNG image", async () => {
            const saveDialog = await setupDialogFunction("showSaveDialog","Enregistrer sous...",false,TestImagePath);
            await waitseconds(0.5);
            let fileExists = false;
            try {
                await access(TestImagePath);
                fileExists = true;
            }catch(e){
            }
            expect(fileExists).toBe(true);
            let image = await readPNG(TestImagePath);
            expect(image.width).toBeGreaterThanOrEqual(734);
            expect(image.height).toBeGreaterThanOrEqual(575);
            expect(checkImage(image)).toBe(true);
        });
        
    });
    
    test.describe("Opening PNG file", () => {
        test.beforeAll(async () => {
            window = await electronApp.firstWindow();
        })
        test("Using \"Ouvrir...\" menu should draw image in Canvas", async () => {
            await setupDialogFunction("showOpenDialog","Ouvrir...",false,iconPath);
            await waitseconds(0.5);
            const base64Content = await window.locator("canvas").evaluate(c => {
                const url = c.toDataURL('image/png');
                return url.substring(url.indexOf(",") + 1);
            });
            await writeFile(TestImagePath,base64Content,{encoding : "base64"});
            const refImage = await readPNG(iconPath);
            const image = await readPNG(TestImagePath);
            expect(compareImages(refImage,image)).toBe(true);
        });
    });
})


test.describe("Best practices", () => {
    test("Closing all windows should exit app except on mac", async () => {
        electronApp = await electron.launch({ args: ['.'] });
        const closeListener = await electronApp.evaluate(({app}) => {
            let listeners = app.listeners("window-all-closed");
            if(listeners.length < 2){
                return null;
            }
            return listeners[1].toString();
        });
        expect(closeListener).not.toBeNull();
        expect(closeListener.includes("darwin") || closeListener.toLowerCase().includes("mac")).toBe(true);
        expect(closeListener).toContain("app.quit()");
        electronApp.close();
    });
    test("Activating App on MacOS with all windows closed should reopen a window", async () => {
        electronApp = await electron.launch({ args: ['.'] });
        const activateListener = await electronApp.evaluate(({app}) => {
            let listeners = app.listeners("activate");
            if(listeners.length < 1){
                return null;
            }
            return listeners[0].toString();
        });
        expect(activateListener).not.toBeNull();
        electronApp.close();
    });
    test.describe("Avoid using dangerous APIs", () => {
        test("App should not use remote, nodeIntegration or disable contextIsolation", async () => {
            const packageJSON = await readFile(resolve(__dirname,"../package.json"),{encoding : "utf-8"});
            expect(packageJSON).not.toContain("@electron/remote");
            const main = await readFile(resolve(__dirname,"../main.js"),{encoding : "utf-8"});
            expect(main).not.toContain("nodeIntegration");
            expect(main).not.toContain("contextIsolation");
        });
    });
});

const setupDialogFunction = async (functionName,menu,cancelled,file) => {
        return electronApp.evaluate(async ({app,dialog,Menu,BrowserWindow},{functionName,canceled,file,menu}) => {
            let hasBeenCalled = false;
            let parented = null;
            let dialogOptions = {};
            dialog[functionName] = (parent,options) => {
                hasBeenCalled = true;
                parented = parent === BrowserWindow.getFocusedWindow();
                dialogOptions = options;
                return Promise.resolve({canceled, filePaths : canceled ? [] : [file], filePath : canceled ? undefined : file});
            }
            app.getSubMenu(Menu.getApplicationMenu(),"Fichier",menu).click();
            return {hasBeenCalled,options : dialogOptions,parented};
        }, {functionName,cancelled,file,menu});
    
}

const readPNG = (filePath) => {
    return new Promise(async (res,rej) => {
        let file = await open(filePath);
        let metaData = null;
        file.createReadStream().pipe(new PNG({}))
        .on("metadata", (meta) => {
            metaData = meta;
        })
        .on("parsed",(data) => {
            res({data,...metaData});
        }).on("error",(e) => {
            rej(e);
        });
    });
}

const checkImage = ({data}) => {
    for(let i = 0; i < data.length; i++){
        if(data[i] !== 255){
            return false;
        }
    }
    return true;
}

const compareImages = ({refData,refWidth,refHeight},{data}) => {
    for(let y = 0; y < refHeight; y++){
        for(let x = 0; x < refWidth; x++){
            let idx = (this.width * y + x) << 2;
            if(data[idx] !== refData[idX] || data[idx + 1] !== refData[idX + 1] ||  data[idx + 2] !== refData[idX + 2 ]){
                return false;
            }
        }
    }
    return true;
}

const waitseconds = (secs) => {
    return new Promise((res) => {
        setTimeout(res,secs*1000);
    });
}